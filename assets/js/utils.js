import Noty from 'noty';


export default function ajaxPostForm(event) {
  event.preventDefault();
  const form = $(event.currentTarget);
  $.ajax({
    url: form.attr('action'),
    type: 'POST',
    data: form.serialize(),
  }).done((response) => {
    if (response.errors != null && Object.keys(response.errors).length > 0) {
      const errors = Object.keys(response.errors).map(errorField => response.errors[errorField]);
      new Noty({
        type: 'error',
        layout: 'topCenter',
        text: errors.flat().join('<br>'),
        timeout: 3000,
      }).show();
    } else if (response.redirect != null) {
      window.location.href = response.redirect;
    }
  });
}
