$('#photo_file').change(() => {
  const input = document.getElementById('photo_file');
  if (input.files && input.files[0]) {
    const reader = new FileReader();
    if ($('#imgPrev').length === 0) {
      $('#previewContainer').html('<img id="imgPrev" src="" style="width:100%;height:100%;" alt="Превью">');
    }
    reader.onload = (e) => {
      $('#imgPrev').attr('src', e.target.result);
    };
    reader.readAsDataURL(input.files[0]);
  }
});
