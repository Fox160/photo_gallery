import MobileDetect from 'mobile-detect';

const nav = $('nav');
const icon = $('#mainIcon');

let isHide = true;
const md = new MobileDetect(navigator.userAgent);
if (md.mobile() != null) {
  nav.hide();
  icon.click(() => {
    if (isHide) {
      nav.show();
    } else {
      nav.hide();
    }
    isHide = !isHide;
  });
}
