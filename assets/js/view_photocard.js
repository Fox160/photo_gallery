require('jquery');

const csrfToken = $('meta[name=csrf-token]').attr('content');

$('#deleteButton').click((event) => {
  const photocardId = $(event.currentTarget).data('id');
  $.ajax({
    url: `/album/photocard/${photocardId}/delete`,
    type: 'DELETE',
    cache: false,
    data: { csrf_token: csrfToken },
  }).done((response) => {
    if (response.redirect) {
      window.location.href = response.redirect;
    }
  });
});
