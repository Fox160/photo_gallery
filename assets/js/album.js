const pagination = 10;
let currentCount = 0;
const addPhotoRow = $('#addPhotoRow');
const albumRow = $('#albumRow');
addPhotoRow.hide();


function loadTen() {
  $.get('/album/photocards', (photocardsId) => {
    if (currentCount === 0) {
      addPhotoRow.show();
      albumRow.html('');
    }

    let nextCount = currentCount + pagination;
    if (photocardsId.length <= nextCount) {
      nextCount = photocardsId.length;
    }

    $.each(photocardsId.slice(currentCount, nextCount), (index, photocardId) => {
      $.ajax({
        method: 'GET',
        url: `/album/photocard/${photocardId}/card`,
      }).done((photocardHTML) => {
        albumRow.append(photocardHTML);
      });
    });
    currentCount += nextCount;
  });
}

loadTen();
$(window).scroll(() => {
  if ($(window).scrollTop() + $(window).height() + 100 >= $(document).height()) {
    loadTen();
  }
});
