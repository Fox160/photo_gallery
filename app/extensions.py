from flask_bcrypt import Bcrypt
from flask_caching import Cache
from flask_debugtoolbar import DebugToolbarExtension
from flask_json import FlaskJSON
from flask_login import LoginManager
from flask_mail import Mail
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy
from flask_uploads import UploadSet, IMAGES
from flask_webpack import Webpack
from flask_wtf.csrf import CSRFProtect


bcrypt = Bcrypt()
cache = Cache()
debug_toolbar = DebugToolbarExtension()
json = FlaskJSON()
login_manager = LoginManager()
csrf_protect = CSRFProtect()
webpack = Webpack()
db = SQLAlchemy()
migrate = Migrate()
photos_set = UploadSet('photos', IMAGES)
mail = Mail()
