import os

import environs


env = environs.Env()
env.read_env()

ENV = env.str('FLASK_ENV', default='production')
DEBUG = ENV == 'development'
SECRET_KEY = env.str('SECRET_KEY')

APP_DIR = os.path.abspath(os.path.dirname(__file__))
PROJECT_ROOT = os.path.abspath(os.path.join(APP_DIR, os.pardir))

BCRYPT_LOG_ROUNDS = env.int('BCRYPT_LOG_ROUNDS', default=13)

DEBUG_TB_ENABLED = DEBUG
DEBUG_TB_INTERCEPT_REDIRECTS = False

CACHE_TYPE = 'simple'

JSON_ADD_STATUS = True
JSON_DATETIME_FORMAT = '%d/%m/%Y %H:%M'
JSON_USE_ENCODE_METHODS = True

SQLALCHEMY_TRACK_MODIFICATIONS = False
SQLALCHEMY_DATABASE_URI = env.str('DATABASE_URL', default='sqlite://')
SQLALCHEMY_STORAGE_FILE = env.str('STORAGE_FILE', default=None)

WEBPACK_MANIFEST_PATH = os.path.join(APP_DIR, 'webpack', 'manifest.json')

UPLOADED_PHOTOS_DEST = env.str('DATASTORAGE_PATH')
if not os.path.exists(UPLOADED_PHOTOS_DEST):
    os.makedirs(UPLOADED_PHOTOS_DEST)

MAIL_SERVER = 'smtp.yandex.ru'
MAIL_PORT = 465
MAIL_USE_TLS = False
MAIL_USE_SSL = True
MAIL_USERNAME = 'ksenija-kstenk@yandex.ru'
MAIL_PASSWORD = 'vk.ru160'
