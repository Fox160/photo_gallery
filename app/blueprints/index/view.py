import os
from flask import Blueprint, render_template


blueprint = Blueprint('index', __name__, static_folder=os.path.join(os.pardir, 'static'))


@blueprint.route('/', methods=['GET'])
def index():
    return render_template('index/index.html')
