import os
from flask import Blueprint, render_template


blueprint = Blueprint('advantages', __name__, static_folder=os.path.join(os.pardir, 'static'))


@blueprint.route('/advantages', methods=['GET'])
def advantages():
    return render_template('advantages/advantages.html')
