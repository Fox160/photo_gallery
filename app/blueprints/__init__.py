from . import index, about, advantages, album, contacts, authorization

__all__ = ['index', 'about', 'advantages', 'album', 'contacts', 'authorization']
