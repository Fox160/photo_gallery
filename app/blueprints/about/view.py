import os
from flask import Blueprint, render_template


blueprint = Blueprint('about', __name__, static_folder=os.path.join(os.pardir, 'static'))


@blueprint.route('/about', methods=['GET'])
def about():
    return render_template('about/about.html')
