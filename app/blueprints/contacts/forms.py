from flask_wtf import FlaskForm
from wtforms import StringField, TextAreaField
from wtforms.fields.html5 import EmailField
from wtforms.validators import DataRequired, Email


class ContactsForm(FlaskForm):
    username = StringField('Ваше имя:', validators=[DataRequired()])
    email = EmailField('Email:', validators=[DataRequired(), Email(message='Неверный адрес электронной почты')])
    message = TextAreaField('Комментарий:', validators=[DataRequired()])
