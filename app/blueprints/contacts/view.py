import os

from flask import Blueprint, render_template, request, current_app
from flask_json import json_response
from flask_login import current_user
from flask_mail import Message

from app.blueprints.contacts.forms import ContactsForm
from app.extensions import mail

blueprint = Blueprint('contacts', __name__, static_folder=os.path.join(os.pardir, 'static'))


def send_email(email: str, message: str, username: str):
    admin_mail = current_app.config['MAIL_USERNAME']
    msg = Message(
        subject=f'Письмо от пользователя {username} {email}',
        sender=admin_mail,
        recipients=[admin_mail],
        body=message
    )
    mail.send(msg)


@blueprint.route('/contacts', methods=['GET', 'POST'])
def contacts():
    form = ContactsForm(request.form)
    if request.method == 'POST':
        if form.validate_on_submit():
            send_email(form.email.data, form.message.data, form.username.data)
        return json_response(errors=form.errors)

    if not current_user.is_anonymous:
        form.username.data = current_user.name
        form.username.render_kw = {'readonly': True}
        form.email.data = current_user.email
        form.email.render_kw = {'readonly': True}
    return render_template('contacts/contacts.html', form=form)
