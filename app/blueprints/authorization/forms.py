from flask_wtf import FlaskForm
from wtforms import PasswordField, StringField, SubmitField
from wtforms.validators import DataRequired, Email, EqualTo, Length
from wtforms.fields.html5 import EmailField

from app.database import User


class LoginForm(FlaskForm):
    email = EmailField('Email', validators=[DataRequired(), Email(message='Неверный адрес электронной почты')])
    password = PasswordField('Пароль', validators=[DataRequired()])
    submit = SubmitField('Войти')

    def __init__(self, *args, **kwargs):
        super(LoginForm, self).__init__(*args, **kwargs)
        self.user = None

    def validate(self) -> bool:
        if not super(LoginForm, self).validate():
            return False

        self.user = User.query.filter_by(email=self.email.data).first()
        if not self.user or self.user.deleted:
            self.email.errors.append('Неизвестный пользователь')
            return False

        if not self.user.check_password(self.password.data):
            self.password.errors.append('Неправильный пароль')
            return False

        return True


class RegisterForm(FlaskForm):
    name = StringField('Имя', validators=[DataRequired()])
    email = EmailField('Email', validators=[DataRequired(), Email(message='Неверный адрес электронной почты')])
    password = PasswordField('Пароль', validators=[DataRequired(), Length(min=6, message='Слишком короткий пароль')])
    password_confirm = PasswordField('Повторите пароль', [DataRequired(), EqualTo('password', message='Пароли не совпадают')])
    submit = SubmitField('Зарегистрироваться')

    def __init__(self, *args, **kwargs):
        super(RegisterForm, self).__init__(*args, **kwargs)
        self.user = None

    def validate(self) -> bool:
        if not super(RegisterForm, self).validate():
            return False

        if User.query.filter_by(email=self.email.data).count():
            self.email.errors.append('Такой пользователь уже существует')
            return False
        return True
