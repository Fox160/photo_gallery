import os
from flask import Blueprint, render_template, request, redirect, url_for
from flask_login import login_required, login_user, logout_user
from flask_json import json_response

from app.database import User
from app.extensions import login_manager
from .forms import LoginForm, RegisterForm


blueprint = Blueprint('authorization', __name__, static_folder=os.path.join(os.pardir, 'static'))


@login_manager.user_loader
def load_user(user_id):
    return User.query.get(user_id)


@blueprint.route('/sign_in', methods=['GET', 'POST'])
def sign_in():
    form = LoginForm(request.form)
    if request.method == 'POST':
        if form.validate_on_submit():
            login_user(form.user)
        return json_response(redirect=url_for('index.index'), errors=form.errors)
    return render_template('authorization/sign_in.html', form=form)


@blueprint.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('index.index'))


@login_manager.unauthorized_handler
def unauthorized_callback():
    return redirect(url_for('authorization.sign_in'))


@blueprint.route('/registration', methods=['GET', 'POST'])
def registration():
    form = RegisterForm(request.form)
    if request.method == 'POST':
        if form.validate_on_submit():
            User.create(name=form.name.data, email=form.email.data, password=form.password.data)
        return json_response(redirect=url_for('authorization.sign_in'), errors=form.errors)
    return render_template('authorization/registration.html', form=form)
