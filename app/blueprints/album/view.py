import os

from flask import Blueprint, render_template, request, url_for, redirect, send_file
from flask_login import login_required, current_user
from flask_json import as_json
from werkzeug.datastructures import CombinedMultiDict

from app.database import Photo
from app.extensions import photos_set
from .forms import AddPhotoCardForm, EditPhotoCardForm


blueprint = Blueprint('album', __name__, static_folder=os.path.join(os.pardir, 'static'))


@blueprint.route('/album', methods=['GET'])
@login_required
def album():
    return render_template('album/album.html')


@blueprint.route('/album/photocards', methods=['GET'])
@login_required
@as_json
def get_photocards_id():
    photos = Photo.query \
        .filter_by(deleted=False) \
        .filter_by(user_id=current_user.id) \
        .order_by(Photo.create_datetime.desc()) \
        .all()
    return [photo.id for photo in photos]


@blueprint.route('/album/photocard/<uuid>/card', methods=['GET'])
@login_required
def get_photocard_card(uuid: str):
    photo = Photo.query.filter_by(deleted=False).filter_by(id=uuid).first_or_404()
    return render_template('album/photocard.html', photo=photo)


@blueprint.route('/album/photocard/add', methods=['GET', 'POST'])
@login_required
def add_photocard():
    form = AddPhotoCardForm(CombinedMultiDict((request.files, request.form)))
    if request.method == 'POST':
        if form.validate_on_submit():
            photo = Photo.create(user_id=current_user.id, description=form.description.data)
            filename = photos_set.save(form.photo_file.data, folder=str(photo.user_id), name=f'{str(photo.id)}.')
            name, extension = os.path.splitext(os.path.basename(filename))
            photo.update(extension=extension.lstrip('.'))
            return redirect(request.args.get('next') or url_for('album.album'))
    return render_template('album/edit_photocard.html', form=form)


@blueprint.route('/album/photocard/<uuid>', methods=['GET'])
@login_required
def get_photocard(uuid: str):
    photo = Photo.query.filter_by(deleted=False).filter_by(id=uuid).first_or_404()
    return render_template('album/view_photocard.html', photo=photo)


@blueprint.route('/album/photocard/<uuid>/edit', methods=['GET', 'POST'])
@login_required
def edit_photocard(uuid: str):
    photo = Photo.query.filter_by(deleted=False).filter_by(id=uuid).first_or_404()
    form = EditPhotoCardForm(request.form)
    if request.method == 'POST':
        if form.validate_on_submit():
            photo.update(description=form.description.data)
            return redirect(request.args.get('next') or url_for('album.album'))

    form.photo = photo
    form.description.data = photo.description
    return render_template('album/edit_photocard.html', form=form)


@blueprint.route('/album/photocard/<uuid>/delete', methods=['DELETE'])
@login_required
@as_json
def delete_photocard(uuid: str):
    photo = Photo.query.get_or_404(uuid)
    photo_path = photos_set.path(photo.name(), folder=str(photo.user_id))
    if os.path.exists(photo_path):
        os.remove(photo_path)
    photo.delete()
    return {'redirect': request.args.get('next') or url_for('album.album')}


@blueprint.route('/album/photo/<uuid>', methods=['GET'])
@login_required
def get_photo(uuid: str):
    photo = Photo.query.filter_by(deleted=False).filter_by(id=uuid).first_or_404()
    return send_file(photos_set.path(photo.name(), folder=str(photo.user_id)))
