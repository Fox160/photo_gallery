from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileRequired, FileAllowed
from wtforms import TextAreaField, SubmitField
from wtforms.validators import DataRequired

from app.extensions import photos_set


class AddPhotoCardForm(FlaskForm):
    photo_file = FileField('Фотография', validators=[FileRequired(), FileAllowed(photos_set)])
    description = TextAreaField('Описание', validators=[DataRequired()])
    submit = SubmitField('Загрузить')


class EditPhotoCardForm(FlaskForm):
    description = TextAreaField('Описание', validators=[DataRequired()])
    submit = SubmitField('Обновить')
