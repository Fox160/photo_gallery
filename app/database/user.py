import base64
from typing import Optional

from sqlalchemy import Column, Binary, String
from sqlalchemy.orm import relationship
from sqlalchemy_utils import EmailType
from flask_login import UserMixin

from app.extensions import bcrypt
from .base_model import BaseModel


class User(BaseModel, UserMixin):
    __tablename__ = 'users'

    name = Column(String(255))
    email = Column(EmailType(), unique=True, nullable=False, index=True)
    password = Column(Binary(128), nullable=False)
    photos = relationship('Photo', back_populates='user', lazy='dynamic')

    def __init__(self, name:str, email: str, password: str, password_base64: Optional[str] = None, **kwargs):
        super().__init__(name=name, email=email, **kwargs)
        if password_base64:
            self.password = base64.b64decode(password_base64)
        else:
            self.set_password(password)

    def update(self, commit: bool = True, **kwargs) -> 'User':
        for attr, value in kwargs.items():
            if attr == 'password':
                self.set_password(value)
            else:
                setattr(self, attr, value)
        return self.save(commit)

    def set_password(self, password: str):
        self.password = bcrypt.generate_password_hash(password)

    def check_password(self, value) -> bool:
        return bcrypt.check_password_hash(self.password, value)

    @property
    def is_active(self) -> bool:
        return not self.deleted

    def __json__(self) -> dict:
        data = super().__json__()
        data['password'] = base64.b64encode(data['password']).decode('utf-8')
        return data
