import json
from datetime import datetime
from uuid import uuid4

from sqlalchemy import Column, DateTime, Boolean
from sqlalchemy_utils import UUIDType

from app.extensions import db


class BaseModel(db.Model):
    __abstract__ = True
    __table_args__ = {'extend_existing': True}

    id = Column(UUIDType(binary=False), primary_key=True, default=uuid4, nullable=False, unique=True)
    create_datetime = Column(DateTime(), default=datetime.now, nullable=False)
    deleted = Column(Boolean(), default=False, nullable=False, index=True)

    @classmethod
    def create(cls, **kwargs) -> 'BaseModel':
        if 'create_datetime' in kwargs.keys():
            kwargs['create_datetime'] = datetime.fromisoformat(kwargs['create_datetime'])
        return cls(**kwargs).save()

    def update(self, commit: bool = True, **kwargs) -> 'BaseModel':
        for attr, value in kwargs.items():
            setattr(self, attr, value)
        return self.save(commit)

    def save(self, commit: bool = True) -> 'BaseModel':
        db.session.add(self)
        if commit:
            db.session.commit()
        return self

    def delete(self, commit: bool = True, force: bool = False):
        if not force:
            return self.update(commit, deleted=True)
        db.session.delete(self)
        if commit:
            db.session.commit()
        return self

    def __eq__(self, other) -> bool:
        if isinstance(other, BaseModel):
            return self.id == other.id
        return super().__eq__(other)

    def __hash__(self) -> int:
        return hash(self.id)

    def __json__(self) -> dict:
        data = {column.name: getattr(self, column.name) for column in self.__table__.columns}
        data['id'] = str(data['id'])
        data['create_datetime'] = data['create_datetime'].isoformat()
        return data

    def __repr__(self) -> str:
        return json.dumps(self.__json__(), sort_keys=True)
