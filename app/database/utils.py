from sqlalchemy import Column, ForeignKey


def reference_col(table_name: str, nullable: bool = False, pk_name: str = 'id', **kwargs) -> 'Column':
    return Column(
        ForeignKey('{table_name}.{pk_name}'.format(table_name=table_name, pk_name=pk_name)),
        nullable=nullable,
        **kwargs
    )
