from .user import User
from .photo import Photo


tables = [User, Photo]
__all__ = ['tables', 'User', 'Photo']
