from sqlalchemy import Column, String
from sqlalchemy.orm import relationship

from .base_model import BaseModel
from .utils import reference_col


class Photo(BaseModel):
    __tablename__ = 'photos'

    user_id = reference_col('users', nullable=False, index=True)
    user = relationship('User', back_populates='photos')

    description = Column(String(255))
    extension = Column(String(32))

    def name(self) -> str:
        return f'{self.id}.{self.extension}'

    def __json__(self) -> dict:
        data = super().__json__()
        data['user_id'] = str(data['user_id'])
        return data
