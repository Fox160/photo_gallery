import os

from flask import Flask
from sqlalchemy import event
from werkzeug.contrib.fixers import ProxyFix
from flask_uploads import configure_uploads, patch_request_class

from app import blueprints, extensions, database, utils


def create_app(config_object: str = 'app.config'):
    app = Flask(__name__.split('.')[0])
    app.config.from_object(config_object)
    register_extensions(app)
    register_blueprints(app)
    register_shell_context(app)
    app.wsgi_app = ProxyFix(app.wsgi_app)
    return app


def register_extensions(app: Flask):
    extensions.json.init_app(app)
    extensions.db.init_app(app)
    app.before_first_request_funcs.append(extensions.db.create_all)
    storage_file_path = app.config.get('SQLALCHEMY_STORAGE_FILE')
    if storage_file_path:
        if not os.path.exists(os.path.dirname(storage_file_path)):
            os.makedirs(os.path.dirname(storage_file_path))
        if os.path.exists(storage_file_path):
            app.before_first_request_funcs.append(utils.load_from_json)
        event.listens_for(extensions.db.session, 'after_flush_postexec')(utils.save_to_json)
    extensions.migrate.init_app(app, extensions.db)
    extensions.bcrypt.init_app(app)
    extensions.cache.init_app(app)
    extensions.debug_toolbar.init_app(app)
    extensions.csrf_protect.init_app(app)
    extensions.webpack.init_app(app)
    extensions.login_manager.init_app(app)
    configure_uploads(app, extensions.photos_set)
    patch_request_class(app)
    extensions.mail.init_app(app)


def register_blueprints(app: Flask):
    app.register_blueprint(blueprints.index.blueprint)
    app.register_blueprint(blueprints.about.blueprint)
    app.register_blueprint(blueprints.advantages.blueprint)
    app.register_blueprint(blueprints.album.blueprint)
    app.register_blueprint(blueprints.contacts.blueprint)
    app.register_blueprint(blueprints.authorization.blueprint)


def register_shell_context(app: Flask):
    def shell_context():
        return {
            'db': extensions.db,
            'User': database.User,
            'Photo': database.Photo,
        }

    app.shell_context_processor(shell_context)

