import json

from flask import current_app

from app.database import tables


def load_from_json():
    storage_file_path = current_app.config.get('SQLALCHEMY_STORAGE_FILE')
    with open(storage_file_path, 'r') as storage_file:
        data = json.load(storage_file)
    for table in tables:
        for row in data.get(table.__tablename__, []):
            table.create(**row)


def save_to_json(session, flush_context):
    storage_file_path = current_app.config.get('SQLALCHEMY_STORAGE_FILE')
    data = {}
    for table in tables:
        rows = session.query(table).all()
        data[table.__tablename__] = [row.__json__() for row in rows]
    with open(storage_file_path, 'w') as storage_file:
        json.dump(data, storage_file)
