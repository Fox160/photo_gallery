# Photo Gallery

## After clone
```bash
cp .env.develop.example .env
poetry install
npm install
```

## Develop

### Start
```bash
npm start
```

### Run linters
```bash
poetry run prospector
```
